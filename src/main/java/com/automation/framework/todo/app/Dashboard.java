package com.automation.framework.todo.app;


/**
 * @Author - suryajt7
 * TODO: Create a RealTime dashboard which should display pictorial status for below -
 * 1. Overall tests results for all Unit, Integration, UI, Database tests within their respective suites and tags.
 * 2. Environment details where they were run.
 * 3. Screenshots for UI related tests.
 * 4. LogAnalysis stats by analyzing data from Google Analytics and various logs.
 * 5. Integration with Jira to automatically open a custom story, bug or improvement from Dashboard itself.
 * 6. Sankey diagram of all APIs talking to each other at various levels. e.g. http://energyliteracy.com/
 * 7. App and API versioning details.
 * 8. Tech stack with their version details.
 * 9. Generate and display Bucket diagram by analyzing all tests and their tags.
 * 10. Display tests run job schedules
 */
public class Dashboard {

}
